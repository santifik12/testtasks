<?php

namespace Hobby\GraphQl\Controller\Account;

use Magento\Customer\Controller\AbstractAccount;
use Magento\Framework\App\Action\Context;
use Magento\Customer\Model\Session;
use Magento\Framework\Message\ManagerInterface;

class SaveHobby extends AbstractAccount
{
    /**
     * @var Session
     */
    protected $customerSession;

    /**
     * @var ManagerInterface
     */
    protected $messageManager;

    /**
     * @param Context $context
     * @param Session $customerSession
     * @param ManagerInterface $messageManager
     */
    public function __construct(
        Context $context,
        Session $customerSession,
        ManagerInterface $messageManager
    ) {
        parent::__construct($context);
        $this->customerSession = $customerSession;
        $this->messageManager = $messageManager;
    }

    public function execute()
    {
        $customer = $this->customerSession->getCustomer();
        $hobby = $this->getRequest()->getParam('hobby');

        if ($customer && $hobby) {
            $customer->setHobby($hobby);
            $customer->save();

            $this->messageManager->addSuccessMessage(__('Hobby has been saved successfully.'));
        }

        $this->_redirect('hobby/account/edithobby');
    }
}
