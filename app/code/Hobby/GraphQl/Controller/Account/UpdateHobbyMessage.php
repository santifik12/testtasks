<?php
namespace Hobby\GraphQl\Controller\Account;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Customer\Model\Session;

class UpdateHobbyMessage extends Action
{
    /**
     * @var Session
     */
    protected $customerSession;

    /**
     * @var JsonFactory
     */
    protected $jsonFactory;

    /**
     * @param Context $context
     * @param Session $customerSession
     * @param JsonFactory $jsonFactory
     */
    public function __construct(
        Context $context,
        Session $customerSession,
        JsonFactory $jsonFactory
    ) {
        parent::__construct($context);
        $this->customerSession = $customerSession;
        $this->jsonFactory = $jsonFactory;
    }

    /**
     * Execute the AJAX request
     */
    public function execute()
    {
        $result = $this->jsonFactory->create();

        $hobby = $this->customerSession->getCustomer()->getHobby();

        return $result->setData(['hobby' => $hobby]);
    }
}
