<?php

namespace Hobby\GraphQl\Controller\Adminhtml\Index;

use Hobby\GraphQl\Model\HobbyOptionsFactory;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Delete extends Action
{
    protected $resultPageFactory;
    protected $HobbyOptionsFactory;

    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        HobbyOptionsFactory $HobbyOptionsFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->HobbyOptionsFactory = $HobbyOptionsFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $resultRedirectFactory = $this->resultRedirectFactory->create();
        try {
            $id = $this->getRequest()->getParam('option_id');
            if ($id) {
                $model = $this->HobbyOptionsFactory->create()->load($id);
                if ($model->getId()) {
                    $model->delete();
                    $this->messageManager->addSuccessMessage(__("Record Delete Successfully."));
                } else {
                    $this->messageManager->addErrorMessage(__("Something went wrong, Please try again."));
                }
            } else {
                $this->messageManager->addErrorMessage(__("Something went wrong, Please try again."));
            }
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage($e, __("We can't delete record, Please try again."));
        }
        return $resultRedirectFactory->setPath('*/*/index');
    }
}
