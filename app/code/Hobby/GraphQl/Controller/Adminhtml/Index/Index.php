<?php
namespace Hobby\GraphQl\Controller\Adminhtml\Index;

use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Backend\App\Action;

class Index extends Action implements HttpGetActionInterface
{
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    public function execute()
    {
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Hobby_GraphQl::hobby_options_index');
        $resultPage->addBreadcrumb(__('Grid List'), __('Grid List'));
        $resultPage->addBreadcrumb(__('Manage Grid'), __('Manage Grid'));
        $resultPage->getConfig()->getTitle()->prepend(__('Hobby Options'));

        return $resultPage;
    }
}
