<?php

namespace Hobby\GraphQl\Block\Account;

use Magento\Framework\View\Element\Template;
use Magento\Customer\Model\Session;
use Hobby\GraphQl\Model\Customer\Attribute\Source\HobbyOptions;

class EditHobby extends Template
{
    /**
     * @var Session
     */
    protected $customerSession;

    /**
     * @var HobbyOptions
     */
    protected $hobbyOptions;

    /**
     * @param Template\Context $context
     * @param Session $customerSession
     * @param HobbyOptions $hobbyOptions
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        Session $customerSession,
        HobbyOptions $hobbyOptions,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->customerSession = $customerSession;
        $this->hobbyOptions = $hobbyOptions;
    }

    /**
     * Get the current customer's hobby
     *
     * @return string|null
     */
    public function getCustomerHobby()
    {
        return $this->customerSession->getCustomer()->getHobby();
    }

    /**
     * Get available hobby options
     *
     * @return array
     */
    public function getHobbyOptions()
    {
        return $this->hobbyOptions->getAllOptions();
    }
}
