<?php
namespace Hobby\GraphQl\Block\Account;

use Magento\Framework\View\Element\Template;
use Magento\Customer\Model\Session;

class HobbyMessage extends Template
{
    /**
     * @var Session
     */
    protected $customerSession;

    /**
     * @param Template\Context $context
     * @param Session $customerSession
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        Session $customerSession,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->customerSession = $customerSession;
    }

    /**
     * Get the customer's hobby
     *
     * @return string|null
     */
    public function getCustomerHobby()
    {
        return $this->customerSession->getCustomer()->getHobby();
    }

    /**
     * Check if the customer is logged in
     *
     * @return bool
     */
    public function isCustomerLoggedIn()
    {
        return $this->customerSession->isLoggedIn();
    }
}
