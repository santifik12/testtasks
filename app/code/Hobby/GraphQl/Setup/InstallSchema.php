<?php
namespace Hobby\GraphQl\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

class InstallSchema implements InstallSchemaInterface
{
    /**
     * Install method
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();

        $table = $installer->getConnection()->newTable(
            $installer->getTable('hobby_options')
        )->addColumn(
            'option_id',
            Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'nullable' => false, 'primary' => true],
            'Option ID'
        )->addColumn(
            'value',
            Table::TYPE_TEXT,
            255,
            ['nullable' => false],
            'Option Value'
        )->addColumn(
            'label',
            Table::TYPE_TEXT,
            255,
            ['nullable' => false],
            'Option Label'
        )->setComment(
            'Hobby Options Table'
        );

        $installer->getConnection()->createTable($table);

        $installer->endSetup();
    }
}
