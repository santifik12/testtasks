<?php

namespace Hobby\GraphQl\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class InstallData implements InstallDataInterface
{
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        $data = [
            ['value' => 'yoga', 'label' => 'Yoga'],
            ['value' => 'traveling', 'label' => 'Traveling'],
            ['value' => 'hiking', 'label' => 'Hiking'],
        ];

        foreach ($data as $item) {
            $setup->getConnection()->insertForce($setup->getTable('hobby_options'), $item);
        }

        $setup->endSetup();
    }
}
