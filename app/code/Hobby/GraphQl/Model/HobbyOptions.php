<?php
namespace Hobby\GraphQl\Model;

use Magento\Framework\Model\AbstractModel;
use Hobby\GraphQl\Model\ResourceModel\HobbyOptions as ResourceOptions;

class HobbyOptions extends AbstractModel
{
    /**
     * Initialize the model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(ResourceOptions::class);
    }
}
