<?php
namespace Hobby\GraphQl\Model\ResourceModel\HobbyOptions;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Hobby\GraphQl\Model\HobbyOptions;
use Hobby\GraphQl\Model\ResourceModel\HobbyOptions as ResourceOptions;

class Collection extends AbstractCollection
{
    /**
     * Initialize the collection
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            HobbyOptions::class,
            ResourceOptions::class
        );
    }
}
