<?php
namespace Hobby\GraphQl\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class HobbyOptions extends AbstractDb
{
    /**
     * Initialize the resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('hobby_options', 'option_id');
    }
}
