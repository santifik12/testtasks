<?php
namespace Hobby\GraphQl\Model\Customer\Attribute\Source;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;
use Hobby\GraphQl\Model\ResourceModel\HobbyOptions\CollectionFactory;

class HobbyOptions extends AbstractSource
{
    protected $collectionFactory;

    public function __construct(CollectionFactory $collectionFactory)
    {
        $this->collectionFactory = $collectionFactory;
    }

    public function getAllOptions()
    {
        $options = [];
        $collection = $this->collectionFactory->create();
        foreach ($collection as $item) {
            $options[] = ['value' => $item->getValue(), 'label' => $item->getLabel()];
        }
        return $options;
    }

    public function toOptionArray()
    {
        $options = [];
        $collection = $this->collectionFactory->create();
        foreach ($collection as $item) {
            $options[] = ['value' => $item->getValue(), 'label' => $item->getLabel()];
        }
        return $options;
    }
}
