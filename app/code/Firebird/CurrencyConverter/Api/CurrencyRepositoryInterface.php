<?php

namespace Firebird\CurrencyConverter\Api;

interface CurrencyRepositoryInterface
{
    public function updateCurrencyRates();
    public function getCurrencyRates();
    public function convertCurrency($amount, $fromCurrency, $toCurrency);
}
