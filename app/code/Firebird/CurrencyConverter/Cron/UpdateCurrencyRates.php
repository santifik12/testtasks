<?php

namespace Firebird\CurrencyConverter\Cron;

use Firebird\CurrencyConverter\Model\CurrencyRepository;
use Psr\Log\LoggerInterface;

class UpdateCurrencyRates
{
    protected $currencyRepository;
    protected $logger;

    public function __construct(
        CurrencyRepository $currencyRepository,
        LoggerInterface $logger
    ) {
        $this->currencyRepository = $currencyRepository;
        $this->logger = $logger;
    }

    public function execute(): void
    {
        try {
            $this->currencyRepository->beginTransaction();

            $this->currencyRepository->updateCurrencyRates();

            $this->currencyRepository->commit();
            $this->logger->info("Currency rates updated successfully.");
        } catch (\Exception $e) {
            $this->currencyRepository->rollBack();
            $errorMessage = "Error updating currency rates: " . $e->getMessage();
            $this->logger->error($errorMessage);
        }
    }
}
