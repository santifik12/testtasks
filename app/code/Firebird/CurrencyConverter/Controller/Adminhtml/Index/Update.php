<?php
namespace Firebird\CurrencyConverter\Controller\Adminhtml\Index;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Firebird\CurrencyConverter\Model\CurrencyRepository;

class Update extends Action
{
    protected $currencyRepository;

    public function __construct(
        Context $context,
        CurrencyRepository $currencyRepository
    ) {
        parent::__construct($context);
        $this->currencyRepository = $currencyRepository;
    }

    public function execute(): void
    {
        $this->currencyRepository->updateCurrencyRates();
        $this->messageManager->addSuccessMessage(__('Currency rates have been updated.'));
        $this->_redirect('adminhtml/system_config/edit/section/firebird_currency');
    }
}
