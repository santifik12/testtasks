<?php

namespace Firebird\CurrencyConverter\Controller\Adminhtml\Index;

use Magento\Backend\App\Action;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Exception\LocalizedException;
use Firebird\CurrencyConverter\Model\CurrencyRepository;

class Conversion extends Action
{
    protected $currencyRepository;

    protected $sessionManager;

    protected $_coreRegistry;

    public function __construct(
        Action\Context $context,
        CurrencyRepository $currencyRepository
    ) {
        parent::__construct($context);
        $this->currencyRepository = $currencyRepository;
    }

    public function execute()
    {
        $amount = (float)$this->getRequest()->getParam('amount');
        $fromCurrency = $this->getRequest()->getParam('from_currency');
        $toCurrency = $this->getRequest()->getParam('to_currency');

        try {
            $convertedAmount = $this->currencyRepository->convertCurrency($amount, $fromCurrency, $toCurrency);
            if ($convertedAmount) {
                $result = ['result' => $convertedAmount];
            } else {
                $result = ['error' => 'Conversion failed. Please check your input.'];
            }
        } catch (LocalizedException $e) {
            $result = ['error' => $e->getMessage()];
        }

        return $this->resultFactory->create(ResultFactory::TYPE_JSON)->setData($result);
    }
}
