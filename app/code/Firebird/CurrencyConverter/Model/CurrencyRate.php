<?php

namespace Firebird\CurrencyConverter\Model;

use Magento\Framework\Model\AbstractModel;
use Firebird\CurrencyConverter\Model\ResourceModel\CurrencyRate as CurrencyRateResource;

class CurrencyRate extends AbstractModel
{
    protected function _construct()
    {
        $this->_init(CurrencyRateResource::class);
    }
}
