<?php
namespace Firebird\CurrencyConverter\Model;

use Firebird\CurrencyConverter\Api\CurrencyRepositoryInterface;
use Magento\Framework\HTTP\Client\Curl;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Firebird\CurrencyConverter\Model\CurrencyRateFactory;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\App\ResourceConnection;

class CurrencyRepository implements CurrencyRepositoryInterface
{
    protected $baseUrl = 'http://data.fixer.io/api/latest';
    protected $apiKey;
    protected $currencyRates;
    protected $scopeConfig;
    protected $currencyRateFactory;
    protected $curl;
    protected $messageManager;
    protected $resource;
    protected $dbConnection;

    public function __construct(
        ScopeConfigInterface $scopeConfig,
        CurrencyRateFactory $currencyRateFactory,
        ManagerInterface $messageManager,
        Curl $curl,
        ResourceConnection $resource,
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->currencyRateFactory = $currencyRateFactory;
        $this->messageManager = $messageManager;
        $this->curl = $curl;
        $this->resource = $resource;
        $this->dbConnection = $this->resource->getConnection();
    }

    public function beginTransaction(): void
    {
        $this->dbConnection->beginTransaction();
    }


    public function commit(): void
    {
        $this->dbConnection->commit();
    }

    public function rollBack(): void
    {
        $this->dbConnection->rollBack();
    }

    public function updateCurrencyRates(): void
    {
        $this->beginTransaction();

        try {
            $selectedCurrencies = $this->getAvailableCurrencies();

            $apiKey = $this->scopeConfig->getValue('currency/fixerio/api_key');
            $url = $this->baseUrl . '?access_key=' . $apiKey;

            $this->curl->get($url);
            $response = json_decode($this->curl->getBody(), true, 512, JSON_THROW_ON_ERROR);

            if (isset($response['success']) && $response['success'] === true) {
                $rates = $response['rates'];

                $this->currencyRateFactory->create()->getCollection()->walk('delete');

                foreach ($rates as $baseCurrency => $rate) {
                    if (!in_array($baseCurrency, $selectedCurrencies, true)) {
                        continue;
                    }

                    $currencyRate = $this->currencyRateFactory->create();
                    $currencyRate->setBaseCurrency($baseCurrency);
                    $currencyRate->setRate($rate);
                    $currencyRate->save();
                }

                $this->commit();
                $this->messageManager->addSuccessMessage("Currency rates updated successfully.");
            } else {
                $this->rollBack();
                $errorMessage = $response['error']['info'] ?? 'Unknown error';
                $this->messageManager->addErrorMessage("Error with updating currency rates: " . $errorMessage);
            }
        } catch (\Exception $e) {
            $this->rollBack();
            $this->messageManager->addErrorMessage("Error with updating currency rates: " . $e->getMessage());
        }
    }

    public function getCurrencyRates(): array
    {
        $currencyRates = [];
        $currencyRateCollection = $this->currencyRateFactory->create()->getCollection();

        foreach ($currencyRateCollection as $rate) {
            $baseCurrency = $rate->getBaseCurrency();
            $exchangeRate = $rate->getRate();

            $currencyRates[$baseCurrency] = $exchangeRate;
        }

        return $currencyRates;
    }


    public function convertCurrency($amount, $fromCurrency, $toCurrency): array
    {
        $currencyRates = $this->getCurrencyRates();

        /*if (!isset($currencyRates[$fromCurrency], $currencyRates[$toCurrency])) {
            return ['error' => 'Invalid currencies selected'];
        }*/

        $exchangeRateFrom = $currencyRates[$fromCurrency];
        $exchangeRateTo = $currencyRates[$toCurrency];

        $convertedAmount = $amount * ($exchangeRateTo / $exchangeRateFrom);

        return [
            'from_currency' => $fromCurrency,
            'to_currency' => $toCurrency,
            'converted_amount' => $convertedAmount,
        ];
    }

    public function getAvailableCurrencies(): array
    {
        $selectedCurrencies = $this->scopeConfig->getValue('firebird_currency/fixerio/currency_list');

        if ($selectedCurrencies !== null) {
            return explode(',', $selectedCurrencies);
        }

        return [];
    }

}
