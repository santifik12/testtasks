<?php
namespace Firebird\CurrencyConverter\Model\Config\Source;

use Magento\Framework\Data\OptionSourceInterface;

class Currencies implements OptionSourceInterface
{
    public function toOptionArray(): array
    {
        return [
            ['value' => 'USD', 'label' => 'US Dollar'],
            ['value' => 'EUR', 'label' => 'Euro'],
            ['value' => 'GBP', 'label' => 'British Pound'],
            ['value' => 'PLN', 'label' => 'Polish Zloty'],
        ];
    }
}
