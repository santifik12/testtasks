<?php

namespace Firebird\CurrencyConverter\Model\ResourceModel\CurrencyRate;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    protected function _construct()
    {
        $this->_init(
            'Firebird\CurrencyConverter\Model\CurrencyRate',
            'Firebird\CurrencyConverter\Model\ResourceModel\CurrencyRate'
        );
    }
}
