<?php

namespace Firebird\CurrencyConverter\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class CurrencyRate extends AbstractDb
{
    protected function _construct()
    {
        $this->_init('firebird_currency_rates', 'entity_id');
    }
}
