Module for storing and converting currencies. It has a predefined list of currencies
whose rates are downloaded via API for all available currencies and stored in the database.
Also created a page in the admin, where all stored currency rates are displayed and a tool
for their conversion is provided.
