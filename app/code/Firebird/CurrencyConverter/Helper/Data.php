<?php

namespace Firebird\CurrencyConverter\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Backend\Model\UrlInterface;


class Data extends AbstractHelper {

    protected $_adminUrl = null;

    public function __construct(
        Context $context,
        UrlInterface $backendUrl
    )
    {
        parent::__construct($context);
        $this->_adminUrl = $backendUrl;
    }


    public function getUrl($path, $params = false): string
    {
        if ($params)
        {
            return $this->_urlBuilder->getUrl($path, $params);
        }

        return $this->_urlBuilder->getUrl($path);
    }


    public function getUrlAdmin($path, $params = false): string
    {
        if ($params)
        {
            return $this->_adminUrl->getUrl($path, $params);
        }

        return $this->_adminUrl->getUrl($path);
    }

}
