<?php
namespace Firebird\CurrencyConverter\Block\Adminhtml\System\Config;

use Magento\Backend\Block\Template\Context;
use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\Data\Form\Element\AbstractElement;
use Firebird\CurrencyConverter\Model\CurrencyRepository;

class CurrencyRates extends Field
{
    protected $_template = 'Firebird_CurrencyConverter::system/config/currency_rates.phtml';

    protected $currencyRepository;

    public function __construct(
        Context $context,
        CurrencyRepository $currencyRepository,
        array $data = []
    ) {
        $this->currencyRepository = $currencyRepository;
        parent::__construct($context, $data);
    }

    protected function _getElementHtml(AbstractElement $element): string
    {
        return $this->_toHtml();
    }

    public function getCurrencyRates(): array
    {
        return $this->currencyRepository->getCurrencyRates();
    }




}
