<?php

namespace Firebird\CurrencyConverter\Block\Adminhtml\System\Config;

use Magento\Framework\Data\Form\Element\AbstractElement;
use Firebird\CurrencyConverter\Helper\Data as Helper;
use Magento\Backend\Block\Template\Context;


class RefreshRates extends \Magento\Config\Block\System\Config\Form\Field {

    protected $fcHelper;

    public function __construct(
        Helper $fcHelper,
        Context $context,
        array $data = [])
    {
        parent::__construct($context, $data);
        $this->fcHelper = $fcHelper;
    }


    public function getRunNowUrl(): string
    {
        return $this->fcHelper->getUrlAdmin('firebird/index/update', ['from_config' => 1]);
    }

    public function getButtonHtml(): string
    {
        $button = $this->getLayout()->createBlock('Magento\Backend\Block\Widget\Button')->setData(
            [
                'id'       => 'firebird_currency_button',
                'label'    => __('Import Rates'),
                'on_click' => "setLocation('" . $this->getRunNowUrl() . "')",
            ]
        );

        return $button->toHtml();
    }


    public function render(AbstractElement $element): string
    {
        $element->unsScope()->unsCanUseWebsiteValue()->unsCanUseDefaultValue();
        return parent::render($element);
    }


    protected function _getElementHtml(AbstractElement $element): string
    {
        return $this->_toHtml();
    }


    protected function _prepareLayout(): RefreshRates|static
    {
        parent::_prepareLayout();
        $this->setTemplate('Firebird_CurrencyConverter::system/config/button.phtml');

        return $this;
    }


}
