<?php
namespace Firebird\CurrencyConverter\Block\Adminhtml\System\Config;

use Magento\Backend\Block\Template\Context;
use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\Data\Form\Element\AbstractElement;
use Magento\Framework\Message\ManagerInterface;
use Firebird\CurrencyConverter\Model\CurrencyRepository;
use Firebird\CurrencyConverter\Helper\Data as Helper;
use Magento\Framework\Session\SessionManagerInterface;

class Conversion extends Field
{

    protected $_template = 'Firebird_CurrencyConverter::system/config/currency_conversion.phtml';

    protected $messageManager;

    protected $currencyRepository;

    protected $fcHelper;

    protected $sessionManager;

    public function __construct(
        Context $context,
        ManagerInterface $messageManager,
        CurrencyRepository $currencyRepository,
        Helper $fcHelper,
        SessionManagerInterface $sessionManager,
        array $data = []
    ) {
        $this->messageManager = $messageManager;
        $this->currencyRepository = $currencyRepository;
        $this->fcHelper = $fcHelper;
        $this->sessionManager = $sessionManager;
        parent::__construct($context, $data);
    }

    public function getConvertUrl(): string
    {
        return $this->fcHelper->getUrlAdmin('firebird/index/conversion', ['from_config' => 1]);
    }

    protected function _getElementHtml(AbstractElement $element): string
    {
        return $this->_toHtml();
    }

    public function getAvailableCurrencies(): array
    {
        return $this->currencyRepository->getAvailableCurrencies();
    }

}
